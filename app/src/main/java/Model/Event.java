package Model;

import java.util.Date;

/**
 * Created by su on 13/10/17.
 */

public class Event {
    private int id;
    private int id_organisasi;
    private int id_author;
    private String judul;
    private Date waktu;
    private String deskripsi;
    private String tempat;

    public Event(int id, int id_organisasi, int id_author,
                 String judul, Date waktu, String deskripsi, String tempat){
        this.id=id;
        this.id_organisasi=id_organisasi;
        this.id_author=id_author;
        this.judul=judul;
        this.waktu=waktu;
        this.deskripsi=deskripsi;
        this.tempat=tempat;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_organisasi() {
        return id_organisasi;
    }

    public void setId_organisasi(int id_organisasi) {
        this.id_organisasi = id_organisasi;
    }

    public int getId_author() {
        return id_author;
    }

    public void setId_author(int id_author) {
        this.id_author = id_author;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public Date getWaktu() {
        return waktu;
    }

    public void setWaktu(Date waktu) {
        this.waktu = waktu;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }
}
